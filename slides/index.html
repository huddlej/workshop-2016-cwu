<!doctype html>
<html lang="en">

	<head>
		<meta charset="utf-8">

		<title>An introduction to programming</title>

		<meta name="description" content="An introduction to programming">
		<meta name="author" content="John Huddleston">

		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

		<link rel="stylesheet" href="css/reveal.css">
		<link rel="stylesheet" href="css/theme/black.css" id="theme">

		<!-- Code syntax highlighting -->
		<link rel="stylesheet" href="lib/css/zenburn.css">

		<!-- Printing and PDF exports -->
		<script>
			var link = document.createElement( 'link' );
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
			document.getElementsByTagName( 'head' )[0].appendChild( link );
		</script>

		<!--[if lt IE 9]>
		<script src="lib/js/html5shiv.js"></script>
		<![endif]-->
	</head>

	<body>

		<div class="reveal">

			<!-- Any section element inside of this container is displayed as a slide -->
			<div class="slides">
				<section>
					<h1>An introduction to programming</h1>
					<h3>John Huddleston</h3>
					<p>
                        huddlej@gmail.com<br />
						<a href="https://bitbucket.org/huddlej/workshop-2016-cwu/wiki/Home">bitbucket.org/huddlej/workshop-2016-cwu/wiki/</a>
					</p>
				</section>
                <section>
                    <h2>Why are we here?</h2>
                    <ul>
                        <li class="fragment"><strong>Learn</strong> the basics of programming</li>
                        <li class="fragment"><strong>Explore</strong> a scientific programming environment</li>
                        <li class="fragment"><strong>Experiment</strong> with existing tools for data analysis</li>
                        <li class="fragment"><Strong>Plan</strong> to use programming to solve real problems</li>
                    </ul>
                </section>
                <section>
                    <h2>Who am I?</h2>
                    <ul>
                        <li>Scientist in a human genetics lab at the University of Washington</li>
                        <li>Professional programmer for 8 years</li>
                        <li>M.S. in computer science and biology</li>
                        <li>Husband, father, and nerd.</li>
                    </ul>
                </section>
                <section>
                    <h2>Who are you?</h2>
                    <ul>
                        <li>Psychology Club members?</li>
                        <li>Students in Dr. Pereira's class?</li>
                        <li>Eager future programmers and/or scientists?</li>
                    </ul>
                </section>
				<section>
					<h2>The plan for today</h2>
                    <ol>
                        <li>Introductions</li>
                        <li>Communicating with programs</li>
                        <li>Learning the basics of Python</li>
                        <li>Exploring data with Python</li>
                    </ol>
				</section>
                <section>
                    <h2>Outcomes for this workshop</h2>

                    <p>At the end of this workshop you will know how to:</p>
                    <ul>
                        <li>Communicate ideas as programs</li>
                        <li>Write basic code in a widely used programming language</li>
                        <li>Explore your data in an online interactive notebook</li>
                        <li>Find additional online and print resources to learn more</li>
                    </ul>
                </section>
                <section>
                    <h2>Why program?</h2>

                    <ul>
                        <li>Automate repetitive and/or intricate tasks</li>
                        <li>Reduce errors from manual analyses</li>
                        <li>Enable iterative analyses that build on previous work</li>
                        <li>Document analyses for yourself and others</li>
                    </ul>
                </section>
                <section>
                    <h2>Communicating with programs</h2>

                    <ul>
                        <li class="fragment">In the next 5 minutes, write a "recipe" for something you know how to do.</li>
                        <li class="fragment">Partner up, exchange recipes, and independently name what your partner's recipe describes on the back of the card.</li>
                        <li class="fragment">Review your recipes together.</li>
                    </ul>
                </section>
                <section>
                    <h2>Recipe review</h2>
                    <p>What did we learn from reading someone else's receipe?</p>
                    <div class="fragment" style="width: 50%; float: left">
                    <ul>
                        <li>Language</li>
                        <li>Vocabulary</li>
                        <li>Handwriting</li>
                        <li>Level of detail</li>
                    </ul>
                    </div>
                    <div class="fragment" style="width: 50%; float: left">
                    <ul>
                        <li>Recipe name</li>
                        <li>Ingredients</li>
                        <li>Outcomes</li>
                        <li>Anything else?</li>
                    </ul>
                    </div>
                </section>
                <section>
                    <h2>Programming is communication</h2>

                    <ul>
                        <li>We program to communicate to computers, people, and other programs</li>
                        <li class="fragment">Programming requires languages with syntax and semantics</li>
                        <li class="fragment">There are <a href="https://en.wikipedia.org/wiki/List_of_programming_languages_by_type">hundreds of languages</a></li>
                        <li class="fragment">We will focus on "pseudo-code" and Python</li>
                    </ul>
                </section>
                <section>
                    <h2>Pseudo-code recipes</h2>

                    <p>Pseudo-code is any semi-structured flow of data and logic.</p>

                    <pre class="fragment"><code class="bash" data-trim data-noescape>
                        Name: make cereal
Steps:
    get bowl
    get cereal
    while bowl is not full
        pour cereal

    get milk
    while cereal is visible
        pour milk

    get spoon
    while bowl is not empty
        use spoon to eat cereal

    if you like to drink the milk
        drink milk from bowl
    else
        offer bowl of milk to the dog

    for dish in bowl and spoon
        clean dish
                    </code></pre>
                </section>
                <section>
                    <h2>Real code recipes</h2>

                    <p>Real code is a formally defined flow of data and logic.</p>
                    <pre><code class="python" data-trim data-noescape>
def make_cereal(person, bowl, cereal, milk, spoon, dog):
    while not bowl.is_full:
        cereal.pour_into(bowl)

    while cereal.is_visible:
        milk.pour_into(bowl)

    while not bowl.is_empty:
        spoon.fill_from(bowl)
        person.eat_from(spoon)

    if person.prefers_to_drink_milk:
        person.drink_from(bowl)
    else:
        person.give(bowl, dog)

    for dish in (bowl, spoon):
        dish.clean()
                    </code></pre>
                </section>
                <section>
                    <h2>Write your own pseudo-code</h2>

                    <p>With your partner, write pseudo-code for calculating the mean for a list of numbers.</p>
                </section>
                <section>
                    <h2>Write your own Python code</h2>

                    <p>Login to <a href="https://wakari.io/" target="_new">the online notebook</a>.</p>
                </section>
                <section>
                    <h2>Basics of Python</h2>

                    <ul>
                        <li class="fragment">Data types</li>
                        <li class="fragment">Logic</li>
                        <li class="fragment">Loops</li>
                        <li class="fragment">Functions</li>
                    </ul>
                </section>
                <section>
                    <h2>Explore data</h2>

                    <ul>
                        <li>Summarize data sets with Pandas</li>
                        <li>Plot data with matplotlib and Seaborn</li>
                        <li>Explore <a href="https://www.dropbox.com/s/4avrh91d3m18sx8/PSY_363_Data_Sets.xlsx?dl=0" target="_new">example data from PSY 363</a></li>
                    </ul>
                </section>
                <section>
                    <h2>Next steps</h2>

                    <ul>
                        <li>Write down one aspect of Python you would like to learn more about.</li>
                        <li>Write how you could use Pandas and Python's plotting tools to prepare analyses for your own data.</li>
                        <li>Explore other people's code (e.g., <a href="http://www.github.com">GitHub</a> or <a href="http://bitbucket.org">BitBucket</a>).</li>
                        <li>Write your own code to solve problems that interest you.</li>
                        <li>Dive into <a href="https://bitbucket.org/huddlej/workshop-2016-cwu/wiki/Home">additional online and print resources</a></li>
                    </ul>
                </section>
                <section>
                    <h2>Thank you!</h2>
                </section>
			</div>
		</div>

		<script src="lib/js/head.min.js"></script>
		<script src="js/reveal.js"></script>

		<script>

			// Full list of configuration options available at:
			// https://github.com/hakimel/reveal.js#configuration
			Reveal.initialize({
				controls: true,
				progress: true,
				history: true,
				center: true,

				transition: 'none', // none/fade/slide/convex/concave/zoom

				// Optional reveal.js plugins
				dependencies: [
					{ src: 'lib/js/classList.js', condition: function() { return !document.body.classList; } },
					{ src: 'plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
					{ src: 'plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
					{ src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
					{ src: 'plugin/zoom-js/zoom.js', async: true },
					{ src: 'plugin/notes/notes.js', async: true }
				]
			});

		</script>

	</body>
</html>
