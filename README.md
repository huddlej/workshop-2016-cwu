# An introduction to programming 
**January 29, 2016**

A workshop for CWU students interested in learning how to program.

## Workshop resources

  - [Online notebook tool](https://wakari.io/)
  - [Slides for the workshop](https://bitbucket.org/huddlej/workshop-2016-cwu/downloads/Introduction%20to%20programming.pdf)
  - [IPython notebook with exercises](https://bitbucket.org/huddlej/workshop-2016-cwu/downloads/Introduction%20to%20data.ipynb)

## Programming resources

  - [Dive Into Python](http://www.diveintopython3.net/) - an online guide to the basics of Python
  - [Official Python documentation](https://docs.python.org/3/)
  - [Introducing Python](http://shop.oreilly.com/product/0636920028659.do) - an excellent introductory book on Python
  - [Software Carpentry](http://software-carpentry.org/) - a non-profit organization devoted to teaching programming to all learners
  - [Jupyter Notebooks](https://try.jupyter.org/) - try out 7 different languages in an interactive notebook format
  - [Anaconda](https://www.continuum.io/downloads) - a free Python distribution that bundles Pandas, matplotlib, and hundreds of other useful scientific software

## Data analysis resources

  - [Pandas documentation](http://pandas.pydata.org/pandas-docs/stable/) - all the details you'd ever need about Pandas
  - [Python for Data Analysis](http://shop.oreilly.com/product/0636920023784.do) - the book on Pandas written by the creator
  - [Plotting in Python with matplotlib](http://matplotlib.org/) - documentation for Python's foremost plotting software
  - [Plotting in Python with Seaborn](http://stanford.edu/~mwaskom/software/seaborn/) - documentation for an up-and-coming plotting program with higher-level functionality